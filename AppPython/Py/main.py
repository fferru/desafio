from cryptography.fernet import Fernet
import os

# Genero la clave y la guardo en un .key
def generate_key():
    key = Fernet.generate_key()
    with open("secret.key", "wb") as key_file:
        key_file.write(key)

# Recupero la key del archivo .key
def load_key():
    return open("secret.key", "rb").read()

# Función de encriptado
def encrypt_file(file_name, key):
    fernet = Fernet(key)
    with open(file_name, "rb") as file:
        file_data = file.read()
    encrypted_data = fernet.encrypt(file_data)
    outDir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "Encriptados")
    if not os.path.exists(outDir):
        os.makedirs(outDir)
    with open(os.path.join(outDir, os.path.basename(file_name)), "wb") as file:
        file.write(encrypted_data)

# Función de desencriptado
def decrypt_file(file_name, key):
    fernet = Fernet(key)
    with open(file_name, "rb") as file:
        encrypted_data = file.read()
    decrypted_data = fernet.decrypt(encrypted_data)
    outDir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "Desencriptados")
    if not os.path.exists(outDir):
        os.makedirs(outDir)
    with open(os.path.join(outDir, os.path.basename(file_name)), "wb") as file:
        file.write(decrypted_data)

# Corroboro que exista la key, si no la genero
if not os.path.exists("secret.key"):
    generate_key()
key = load_key()

# Pequeño menú simple por consola, para encriptar es 1 y para desencriptar es 2.
# Se coloca el directorio absoluto si el video no está en el mismo directorio que la app, o la dir relativa si está en el mismo directorio
# Una vez se realiza el encriptado o desencriptado, coloca el correspondiente resultado en las carpetas ./Desencriptados y ./Encriptados
# En el programa de Java habían muchas funcionalidades más, como la elección de borrar/reemplazar el resultado, dónde colocar el resultado, etc.
while True:
    try:
        opcion = int(input("Ingrese 1 para encriptar o 2 para desencriptar: "))
        if opcion == 1:
            print("Ha elegido encriptar...")
            inDir = str(input("Ingresar el directorio del video que desea encriptar: "))
            encrypt_file(inDir, key)
            print("¡Archivo encriptado exitosamente!")
            break
        elif opcion == 2:
            print("Ha elegido desencriptar...")
            inDir = str(input("Ingresar el directorio del video que desea desencriptar: "))
            decrypt_file(inDir, key)
            print("¡Archivo desencriptado exitosamente!")
            break
        else:
            print("El número ingresado no es válido. Por favor, ingrese 1 o 2.")
    except ValueError:
        print("Por favor, ingrese un número válido.")

# Para un trabajo más ordenado y legible hubiera sido mejor tener la correspondiente separación en carpetas con sus funcionalidades, pero al ser 
# sólo una fracción del proyecto original y ser tan pocas funcionalidades, me tomé la libertad de hacerlo todo en el main.py




# En caso de querer usar la misma key que el ejemplo de Java (No probado)
# key_json = {
#     "primaryKeyId": 1658370469,
#     "key": [{
#         "keyData": {
#             "typeUrl": "type.googleapis.com/google.crypto.tink.AesCtrHmacStreamingKey",
#             "keyMaterialType": "SYMMETRIC",
#             "value": "Eg0IgCAQEBgDIgQIAxAgGhCLR6mbaYWa6ZoNGF3/qpZr"
#         },
#         "outputPrefixType": "RAW",
#         "keyId": 1658370469,
#         "status": "ENABLED"
#     }]
# }
# key_bytes = json.dumps(key_json).encode('utf-8')
# key_base64 = base64.b64encode(key_bytes)
# key_for_python = key_base64.decode('utf-8')